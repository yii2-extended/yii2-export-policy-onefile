<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-export-policy-onefile library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use yii\db\ActiveQuery;
use Yii2Extended\Yii2Export\Yii2ExportPolicyOnefile;

/**
 * Yii2ExportPolicyOnefileTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Extended\Yii2Export\Yii2ExportPolicyOnefile
 *
 * @internal
 *
 * @small
 */
class Yii2ExportPolicyOnefileTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Yii2ExportPolicyOnefile
	 */
	protected Yii2ExportPolicyOnefile $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Yii2ExportPolicyOnefile(
			__DIR__,
			new ActiveQuery(static::class),
		);
	}
	
}
