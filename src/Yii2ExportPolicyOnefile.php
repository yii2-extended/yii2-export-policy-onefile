<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-export-policy-onefile library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2Export;

use InvalidArgumentException;
use yii\db\ActiveQueryInterface;

/**
 * Yii2ExportPolicyOneFile class file.
 * 
 * This policy defines only one file to put records in.
 * 
 * @author Anastaszor
 */
class Yii2ExportPolicyOnefile implements Yii2ExportPolicyInterface
{
	
	/**
	 * The file path where to put the data.
	 * 
	 * @var string
	 */
	protected string $_filePath;
	
	/**
	 * The query to get data from the slice of the databse.
	 * 
	 * @var ActiveQueryInterface
	 */
	protected ActiveQueryInterface $_baseQuery;
	
	/**
	 * Whether to continue to iterate over this policy.
	 * 
	 * @var boolean
	 */
	protected $_valid = true;
	
	/**
	 * Builds a new Yii2ExportPolicyOnefile object with the given folder path
	 * and base query.
	 * 
	 * @param string $folderPath
	 * @param ActiveQueryInterface $baseQuery
	 * @throws InvalidArgumentException if the given folder path does not
	 *                                  exists, or is not a directory, or is not writable
	 */
	public function __construct(string $folderPath, ActiveQueryInterface $baseQuery)
	{
		$realPath = \realpath($folderPath);
		if(false === $realPath)
		{
			throw new InvalidArgumentException(\strtr('The given path {path} does not points to a real directory.', ['{path}' => $folderPath]));
		}
		
		if(!\is_dir($realPath))
		{
			throw new InvalidArgumentException(\strtr('The resolved path {path} is not a directory.', ['{path}' => $realPath]));
		}
		
		if(!\is_writable($realPath))
		{
			throw new InvalidArgumentException(\strtr('The resolved path {path} is not writeable.', ['{path}' => $realPath]));
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress NoInterfaceProperties */
		$this->_filePath = $realPath.\DIRECTORY_SEPARATOR.\trim((string) \preg_replace('#\\W+#', '_', (string) $baseQuery->modelClass), '_');
		$this->_baseQuery = $baseQuery;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : ActiveQueryInterface
	{
		return $this->_baseQuery;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		$this->_valid = false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return 1 - (int) $this->_valid;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return $this->_valid;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		$this->_valid = true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Yii2Export\Yii2ExportPolicyInterface::getCurrentQueryId()
	 */
	public function getCurrentQueryId() : string
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress NoInterfaceProperties */
		return \trim((string) \preg_replace('#\\W+#', '-', (string) $this->_baseQuery->modelClass), '-');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Yii2Export\Yii2ExportPolicyInterface::getCurrentActiveQuery()
	 */
	public function getCurrentActiveQuery() : ActiveQueryInterface
	{
		return $this->_baseQuery;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Yii2Export\Yii2ExportPolicyInterface::getCurrentPath()
	 */
	public function getCurrentPath() : string
	{
		return $this->_filePath;
	}
	
}
